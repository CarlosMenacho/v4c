# **Sistema de información sobre la ocupación de buses del transporte publico.**

In this work, we implement an intelligent system to count the number of people getting on in a bus. 

## Update March 28, 2020 21:20
After 5 hours of work, the team V4A achieved the following results:
- We reviewed several state-of-the-art projects. 
- We implemented a person recognition system, see the [video](https://gitlab.com/IsRaTiAl/v4c/-/blob/master/videos/video_metro.avi).

## Tools
This project has been build using the following tools:
```
OpenCV
Tensorflow 2.x
Keras
```

## Resources

* [PCDS](https://freesoft.dev/program/128588362) - The dataset used
* GoogleColab - The virtual machine used in the experiments 

## Acknowledgments
This work has been inspired on: 
* [Benchmark data and method for real-time people counting in cluttered scenes using depth sensors](https://arxiv.org/abs/1804.04339)
* [People Detection and Finding Attractive Areas by the use of Movement Detection Analysis and Deep Learning Approach](https://www.sciencedirect.com/science/article/pii/S1877050919311287)
